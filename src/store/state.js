import mydata from '../../public/data.json'

export default {
  photo: mydata.photo,
  name: mydata.nome,
  job: mydata.job,
  social: mydata.social,
  projects: mydata.projects,
  about: mydata.about,
  contacts: mydata.contacts
}
