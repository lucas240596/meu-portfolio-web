export default {
  setMobile({ commit }, payload) {
    commit('setMobile', payload)
  },

  setNavbarColor({ commit }, payload) {
    commit('setNavbarColor', payload)
  }
}
