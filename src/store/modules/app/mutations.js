export default {
  setMobile(state, payload) {
    state.mobile = payload
  },

  setNavbarColor(state, payload) {
    state.navbarColor = payload
  }
}
