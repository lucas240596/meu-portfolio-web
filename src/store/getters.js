export default {
  getPhoto({ photo }) {
    return photo
  },

  getName({ name }) {
    return name
  },

  getJob({ job }) {
    return job
  },

  getSocial({ social }) {
    return social
  },

  getProjects({ projects }) {
    return projects
  },

  getAbout({ about }) {
    return about
  },

  getContacts({ contacts }) {
    return contacts
  }
}
