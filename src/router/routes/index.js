/** Automatic import routes and export default */

const requireRoutes = require.context('.', true, /\.js$/)
const routes = []

requireRoutes.keys().forEach(fileName => {
  if (fileName === './index.js') return

  const c = requireRoutes(fileName).default
  routes.push(...c)
})

export default routes

