import Vue from 'vue'

const functions = {

  whatsappLink(phoneCode, phone) {
    let regex = /[^0-9]/g
    phoneCode = phoneCode.replace(regex, '')
    phone = phone.replace(regex, '')
    return `https://wa.me/${phoneCode}${phone}`
  },

  emailLink(email) {
    return `mailto:${email}`
  }

}

Vue.prototype.$functions = functions
export default functions
